What is a Data Model?

	A data model describes how data is organized or grouped in a database.

	By creating data models, we can anticipate which data will be managed by the database management system in accordance to the application to be developed.

	A data model is a like blueprint for our database.

Data Modelling Scenario

	Type: Course Booking System,
	Description: Students/Users should be able to book into a course,
	Type of Users: Regular Users, Admin Users, (Instructors),
	hasCourses: true,

	Features:
		-user registration
		-user authentication/login
		-Features of Regular User:
			-view available courses
			-enroll in a course
			-update his details
			-delete his optional details (except for credentials)
		-Features of Admin User:
			-create courses
			-update courses
			-archive courses
			-re-activate courses
			-view all courses

	users and courses as our collections

	users:

	{
		(id)
		username,
		email,
		password,
		isAdmin,
		isActive,
		mobileNo.,
		firstName,
		lastName,
		enrollments: [

			{
				course_id,
				course_name,
				dateEnrolled,
				status,
				isPaid
			}

		],
	}

	courses: 

	{
		(id)
		name,
		description,
		price,
		isActive,
		instructor,
		classSchedule,
		units,
		slotsLeft,
		enrollees: [

			{
				student_id,
				firstName,
				lastName,
				status,
				dateEnrolled
			}

		]

	}

	//Model Relationships

	//One to One - This relationship means that a model is exclusively related to only one model.

	Employee:
	{
		"id": "2021dev"
		"firstName": "Jack",
		"lastName": "Smith",
		"email": "jsmith@gmail.com"
	}

	Credentials:

	{
		"id": "2021dev",
		"role": "dev",
		"team": "tech"
	}

	Embedding Subdocuments - Subdocuments are documents embedded or inside a parent document. 

	user
	{
		"id": "2021dev"
		"firstName": "Jack",
		"lastName": "Smith",
		"email": "jsmith@gmail.com"
		"credentials": {

			"role": "dev",
			"team": "tech"

		}
	}


	One to Many
		One model is related to multiple other models however the the other models are only related to one.

		Person - Many Email Address

		Blog - comments

			A single blog can have multiple comments but each comment should only refer to a single blog.

	blog 

	{
		"id": "blog92921",
		"title": "This is an Awesome Blog",
		"content": "This is an awesome blog I created.",
		"createdOn": "9/29/2021",
		"author": "blogWriter1"
	}

	comment documents

	{
		"id": "blog92921comment1",
		"comment": "Awesome Blog!",
		"author": "peterCritic1",
		"blog_id": "blog92921"
	},
	{
		"id": "blog92921comment2",
		"comment": "Meh. Not an awesome blog.",
		"author": "johnPage12",
		"blog_id": "blog92921"
	}


	Subdocument Array - an array of subdocuments per single document.

	blog with comments subdocument array

	{
		"id": "blog92921",
		"title": "This is an Awesome Blog",
		"content": "This is an awesome blog I created.",
		"createdOn": "9/29/2021",
		"author": "blogWriter1"
		"comments": [

			{
				"id": "blog92921comment1",
				"comment": "Awesome Blog!",
				"author": "peterCritic1",
			},
			{
				"id": "blog92921comment2",
				"comment": "Meh. Not an awesome blog.",
				"author": "johnPage12",
			}


		]
	}


	Many to Many - The relationship of models wherein multiple documents are related to multiple documents

	Books and Authors

	id      name                               price
	1       Grayson Chronicles                 2500
	2       Red Sun                            1500

	id      name
	1       JRR Tolkien
	2       George RR Martin
	3       Not Tee Jae Calinao

	Associative Entity is created when you have a many to many relationship

	books_authors

	id   book_id   author_id
	1    1         1

	//MongoDB - Two Way Embedding. Wherein we can embed details of documents embedded in other documents as these other document's details are also embedded in the other.

	book

	{
		"id": "book1",
		"name": "Grayson Chronicles",
		"description": "A book about the Graysons",
		"price": "2500",
		"authors": [

			{
				"id": "bookAuthor1",
				"author_id": "author1"
			},
			{
				"id": "bookAuthor2",
				"author_id": "author2"
			}


		]

	}

	{
		"id": "book2",
		"name": "Red Sun",
		"description": "What if the sun is red?",
		"price": "1500"
		"authors": [

			{
				"id": "bookAuthor1",
				"author_id": "author1"
			}

		]

	}

	{
		"id": "book3",
		"name": "Red Sun 2",
		"description": "What if the sun is STILL red?",
		"price": "1500"
		"authors": [

			{
				"id": "bookAuthor1",
				"author_id": "author1"
			}

		]

	}

	authors

	{
		"id": "author1",
		"name": "Jeffrie Page",
		"address": {

			"street": "#1 Shakespeare Drive",
			"city": "London",
			"country": "UK"

		}
		"books": [
			{
				"id": "authored_book1",
				"book_id": "book1",
				"writtenOn": "2/12/98"			
			},
			{
				"id": "authored_book2",
				"book_id": "book2",
				"writtenOn": "12/1/99"
			},
			{
				"id": "authored_book3",
				"book_id": "book3",
				"writtenOn": "1/1/03"
			}
		]
	}

	{
		"id": "author2",
		"name": "Mike Chapter",
		"address": {

			"street": "#2 Hamlet Lane",
			"city": "Boston",
			"country": "USA"

		}
		"books": [

			{
				"id": "authored_book1",
				"book_id": "book1",
				"writtenOn": "2/12/98"			
			}


		]
	}

	Entity Relationship Diagrams or ERD
		ERD visualizes the relationship between on model to another.
